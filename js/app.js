(function($) {

	$('body').on('mouseenter', '.openHouse', function(){
		var obj = $(this).parents('.sigil');
		obj.find('.shdw').stop().fadeIn(400);
	}).on('mouseleave', '.openHouse', function(){
		var obj = $(this).parents('.sigil');
		obj.find('.shdw').stop().fadeOut(400);
	});

	setInterval(function(){
		var num = $('body').find('.sigil.down').length;
		if (num == 0)
			$('body').find('.sigil').addClass('down');
		else 
			$('body').find('.sigil').removeClass('down');
	}, 2750);
	

	$('body').on('change', '.spell', function(){
		$(this).parent().find('.fakebox').toggleClass('unchecked').toggleClass('checked');
	});

})(jQuery);